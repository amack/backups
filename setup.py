from setuptools import setup

with open("README.md", "r") as f:
    longdesc = f.read()

"""
Release rules:

    Read in order. If a rule applies to your release, use it, else continue to
    the next rule. Each release type must also follow the rules of semantic
    versioning as described here, substituting SECURITY releases with
    MAINTENANCE: https://packaging.python.org/guides/distributing-packages-using-setuptools/#semantic-versioning-preferred

    1. If a release adds new stable features, it is a MAJOR release.
    2. If a release adds new unstable features or non-security fixes, it is a 
        MINOR release.
    3. If a release adds new security fixes, it is a SECURITY release.

    For version x.y.z, a MAJOR release increments x by 1 and sets y and z to 0.
    A MINOR release increments y by 1 and sets z to 0, and a SECURITY
    release increments z by 1.

"""
setup(
    name="backups",
    version="0.0.0.dev1",
    description="Scripts to facilitate downloading from urls.",
    license="GPLv3",
    long_description=longdesc,
    author="Andrew Mack",
    author_email="andrewmack@benzinga.com",
    maintainer="Andrew Mack",
    maintainer_email="andrewmack@benzinga.com",
    packages=["backups"],
    install_requires=["requests", "apscheduler"]
)

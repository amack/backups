FROM python:3.7.4-slim

RUN mkdir -p /backups
WORKDIR /backups
COPY backups /backups/backups
COPY requirements.txt /backups/requirements.txt
#See requirements.txt file for more info
RUN pip install --trusted-host pypi.python.org -r /backups/requirements.txt

#Run on port 80
#EXPOSE 80

CMD ["python3", "backups", "-f", "$PATH", "-u", "$URLS"]

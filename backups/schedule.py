#!/usr/bin/python

from logging import getLogger 

from apscheduler.schedulers.background import BackgroundScheduler

from dl import download

def schedule(jobs, path):

    log = getLogger(__name__)

    job_defaults = {
        'coalesce': True,
        'max_instances': 3
    }
    sched = BackgroundScheduler(logger=log, job_defaults=job_defaults)

    i = 20 #make this configurable

    for url in jobs:
        if i == 24: #only 0-23 are valid hours
            i = 0
        elif i == 5: #no jobs past 4:00am, make this configurable
            i = 20
        sched.add_job(
                func=download,
                trigger='cron',
                args=[url, path],
                id=url,
                hour=str(i)
                )
        i += 1

    return sched

#!/usr/bin/python

from argparse import ArgumentParser
from os import environ
from os.path import expanduser
from logging import basicConfig
from sys import stdout

from dl import download
from schedule import schedule

def main():
    

    loglvl = 'INFO'
    if 'LOG_LVL' in environ.keys():
        loglvl = environ['LOG_LVL']

    basicConfig(
        format="[%(asctime)s][%(levelname)s]: %(message)s",
        level=loglvl,
        stream=stdout
    )

    parser = ArgumentParser()
    parser.add_argument('-f', '--file', type=str, dest='path', default=None)
    parser.add_argument('-u', '--url', type=str, dest='url', required=True)
    
    args = parser.parse_args()
    
    #Output path generated here. We append the name of the file to this.
    path = "$HOME/backup_data/"
    if 'BACKUP_OUTPUT' in environ.keys():
        path = environ['BACKUP_OUTPUT']
    if args.path is not None:
        path = args.path

    path = expanduser(path)
    
    jobs = None
    if args.url is not None:
        jobs = args.url.split()

    sched = None
    if jobs is not None:
        sched = schedule(jobs, path)

    if sched is not None:
        sched.start()
    else:
        print("No jobs.")

    return 0

import sys

if __name__ == "__main__":
    res = main()
    sys.exit(res)

#!/usr/bin/python

import shutil
from os import environ
from datetime import datetime

import requests

"""
Function: dl sends a request to the url specified by the argument url. This
    request should return a response containing the gzipped file to back up,
    and writes it to disk. 
"""
def download(url, path):
    #generate name of file written to disk
    # /path/filename_date_time.gz
    wtime = datetime.now()
    strtime = wtime.isoformat('_')
    localname = "{p}/{f}_{t}.gz".format(
                                    p=path,
                                    f=url.split('/')[-1],
                                    t=strtime
                                )
    
    buf = 1024
    if environ['BUFFER'] is not None:
        buf = environ['BUFFER']
    
    try:
        with requests.get(url, stream=True) as r:
            with open(localname, 'wb') as f:
                #$BUFFER = number of bytes to write at a time
                shutil.copyfileobj(r.raw, f, buf)
                f.close()
            r.close()
    except Error:
        return False #temp handling

    return True
